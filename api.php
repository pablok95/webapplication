<?php
/*

MySQL database connection
*/
$dbhost='localhost';
$dbname='kotoniak';
$dbuser='kotoniakUser';
$dbpswd='passwordUsera';

// new PDO(link do bazy, użytkownik, hasło, parametry)
$dbSQL = new PDO('mysql:host='.$dbhost.';dbname='.$dbname.';charset=utf8', $dbuser, $dbpswd, array(  
		PDO::ATTR_EMULATE_PREPARES => false,  
		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
		)  
	);


// odebranie danych POST
$postdata = file_get_contents("php://input");

// Zamiana JSON(string) na tablicę 
$data = json_decode($postdata, true);

$data['request']=$_POST['request'];
$data['id']=$_POST['id'];
$data['name']=$_POST['name'];
$data['dtime']=$_POST['dtime'];


if($data['request']=='hello'){
	echo json_encode('Hello to');
}
if($data['request']=='getall'){


	$sql="SELECT * FROM utwory";
	$res = $dbSQL->query($sql)->fetchAll(PDO::FETCH_ASSOC);
	$utwory=array();
	foreach ($res as $value) {
		$utwory[]=array(
			'id'=>$value['id'],
			'name'=>$value['name'],
			'time'=>$value['dtime']
		);
	}

	echo json_encode($utwory);
}
if($data['request']=='getone'){

	$id=$data['id'];
	$sql="SELECT * FROM utwory WHERE id=$id";
	$res = $dbSQL->query($sql)->fetch(PDO::FETCH_ASSOC);
	$utwor=array(
		'id'=>$res['id'],
		'name'=>$res['name'],
		'time'=>$res['dtime']
	);

	echo json_encode($utwor);
}
if($data['request']=='search'){

	$search=$data['search'];
	$sql="SELECT * FROM utwory WHERE name LIKE '%$search%'";
	$res = $dbSQL->query($sql)->fetch(PDO::FETCH_ASSOC);
	$utwor=array(
		'id'=>$res['id'],
		'name'=>$res['name'],
		'time'=>$res['dtime']
	);

	echo json_encode($utwor);
}
if($data['request']=='add'){
	$name=$data['name'];
	$dtime=$data['dtime'];

	$sql="INSERT INTO utwory (name, dtime) VALUES 
 (:name, :dtime)";

	$stmt=$dbSQL->prepare($sql);
	$stmt->bindValue(":name", $name);
	$stmt->bindValue(":dtime", $dtime);
	if($stmt->execute()){
		echo json_encode(array('message'=>'success'));
	}else{
		echo json_encode(array('message'=>'error'));
	}
}
if($data['request']=='edit'){
	$name=$data['name'];
	$dtime=$data['dtime'];
	$id=$data['id'];

	$sql="UPDATE utwory SET name='$name', dtime=$dtime WHERE id=$id";

	if($dbSQL->query($sql)->execute()){
		echo json_encode(array('message'=>'success'));
	}else{
		echo json_encode(array('message'=>'error'));
	}
}
if($data['request']=='delete'){
	$id=$data['id'];

	$sql="DELETE FROM utwory WHERE id=$id";

	if($dbSQL->query($sql)->execute()){
		echo json_encode(array('message'=>'success'));
	}else{
		echo json_encode(array('message'=>'error'));
	}
}
?>