$(document).on("click", ".open-AddSongDialog", function () {
	var mySongId = $(this).data('id');
	$(".modal-body #songId").val( mySongId );
});

$(document).ready(function () {
	$('#my_modal').on('show.bs.modal', function(e) {

		var songId = $(e.relatedTarget).data('id');
		$(e.currentTarget).find('input[name="songId"]').val(songId);
	});

	$(".identifyingClass").click(function () {

		var my_id_value = $(this).data('id');
		$(".modal-body #hiddenValue").val(my_id_value);
	});

	$('#btnShow').on('click', getdata);
});

function editData(id, name, time) {
	$('#form2_id').val(id);
	$('#form2_name').val(name);
	$('#form2_time').val(time);
	$('#edit').fadeIn(300);
}

function show(elem) {
	$('#' + elem).toggle(300)
}
function send() {
	var name = $('#form1_name').val();
	var time = $('#form1_time').val();
	var json = {
		request: "add",
		name: name,
		dtime: time
	}

	var posting = $.post(
		'api.php',
		json,
		function (data) {
			$('#alert1').html(data.message).fadeIn(20);
			$('#form1_name').val('');
			$('#form1_time').val(0);
			getdata();
		},
		"json"
	);
}

function editSong(){
	var name = $('#form2_name').val();
	var time = $('#form2_time').val();
	var id = $('#form2_id').val();
	var json={
	  request:"edit",
	  id: id,
	  name: name,
	  dtime: time
	}
	
	var posting = $.post(
	  'api.php',
	  json,
	  function( data ) {
		$('#edit').hide(20);
		getdata();
	  },
	"json"
	)
  }

  function remove(){
	var id = $('#songId').val();
	console.log(id)
	var json={
		request:"delete",
		id: id,
	  }
	  var posting = $.post(
		'api.php',
		json,
		function( data ) {
		  $('#edit').hide(20);
		  getdata();
		},
	  "json"
	  )
	  $('#addSongDialog').modal('toggle');

  }
  
  function getdata(){
	var posting = $.post(
		'api.php',
		{ request: "getall" },
		function (data) {
			var content = '';
			for (var i = 0; i < data.length; i++) {

				content = content + '<tr><td>' + (i + 1) + '.</td>' +
					'<td><a class="btn" onclick="editData(' + data[i].id + ', ' + '\'' + data[i].name + '\'' + ', ' + data[i].time + ')">' + data[i].name + '</td>' +
					'<td>' + data[i].time + '</td>'+
					'<td> <a type="button"  data-id='+'"'+data[i].id+'"'+ ' data-toggle="modal"  title="Usuń ten utwór" class="open-AddSongDialog btn btn-danger btn-space" href="#addSongDialog">Usuń</a>'+
					'<a type="button"  title="Edytuj ten utwór" class="btn btn-info btn-space" onclick="editData(' + data[i].id + ', ' + '\'' + data[i].name + '\'' + ', ' + data[i].time + ')">Edytuj</a>'+'</tr>'
			}
			$('#content').hide().html(content).show(500);
		},
		"json"
	);
};